var PAGE = 1;

function display10MoreElements(items, page){
  var nbElem = 0;
  if(page*10 > items.length){
    nbElem = items.length % 10;
    $(".buttonscroll").hide();
  }
  else{
    if(page*10 == items.length){
      $(".buttonscroll").hide();
    }

    nbElem = page*10;
  }
  for(i=0;i<nbElem;i++){
    var elem = i + (page-1)*10;
    if(elem == 1){
      $( "<tr/>", {
        html: "<td class='mediumcol titletable'>" + items[elem][0] + "</td><td class='titletable'>" + items[elem][1] +  "</td><td class='pricecol titletable'>" + items[elem][2] +  "</td><td class='shortcol titletable'>" + items[elem][3] + "</td><td class='shortcol titletable'>" + items[elem][4] + "</td>"
      }).appendTo( ".tab" );
    }
    else if(elem != 0){
      $( "<tr/>", {
        html: "<td class='mediumcol'><a href='https://www.pathofexile.com/account/view-profile/" + items[elem][0] + "' target='_blank' rel='noopener noreferrer'>" + items[elem][0] + "</a></td><td>" + items[elem][1] +  "</td><td class='pricecol'>" + items[elem][2] +  "</td><td class='shortcol'>" + items[elem][3]  + "</td><td class='shortcol'>" + items[elem][4] + "</td>"
      }).appendTo( ".tab" );
    }
  }
}

$(function() {
  var data;
  $.ajax({
    type:"GET",
    url:"pnssheet - Feuille 1.csv",
    dataType:"text",
    success: function(response){
      items = $.csv.toArrays(response);

      var nbTabs = (items.length / 10) + 1;
      if(items.length <= 10){
        $(".buttonscroll").hide();
      }

      display10MoreElements(items, PAGE);
    }
  });

  $( ".buttonscroll" ).click(function() {
    PAGE += 1;
    display10MoreElements(items, PAGE);
  });
});
