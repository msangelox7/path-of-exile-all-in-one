var firstDisplay = true;

function animateOrangeToWhite(id){
  document.getElementById(id).classList.add("colorSwap");
  setTimeout(function() {
  document.getElementById(id).classList.remove("colorSwap");
}, 1500);

}

// Shorthand for $( document ).ready()
$(function() {
  $( ".js-togglenav" ).click(function() {
    $(this).children(".thirdnav").slideToggle();
    $(this).toggleClass("active");
  });

  $( ".subnav" ).mouseleave(function() {
    $(".thirdnav").slideUp();
    $(".js-togglenav").removeClass("active");
  });

  // Set the date we're counting down to
  var countDownDate = new Date("Jan 7, 2021 20:00:00").getTime();



  // Update the count down every 1 second
  var x = setInterval(function() {

    // Get today's date and time
    var now = new Date().getTime();

    // Find the distance between now and the count down date
    var distance = countDownDate - now;

    // Time calculations for days, hours, minutes and seconds
    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((distance % (1000 * 60)) / 1000);

    // Display the countdown when loading
    if(firstDisplay == true){
      document.getElementById("days").innerHTML = days;
      animateOrangeToWhite("days");
      document.getElementById("hours").innerHTML = hours;
      animateOrangeToWhite("hours");
      document.getElementById("minutes").innerHTML = minutes;
      animateOrangeToWhite("minutes");

      firstDisplay = false;
    }

    // Modify numbers only when they change
    if(seconds == 59){
      document.getElementById("minutes").innerHTML = minutes;
      animateOrangeToWhite("minutes");
      if(minutes == 59){
        document.getElementById("hours").innerHTML = hours;
        animateOrangeToWhite("hours");
        if(hours == 23){
          document.getElementById("days").innerHTML = hours;
          animateOrangeToWhite("days");
        }
      }
    }

    // Update seconds every ticks
    document.getElementById("seconds").innerHTML = seconds;
    animateOrangeToWhite("seconds");


    // If the count down is finished, write some text
    if (distance < 0) {
      clearInterval(x);
      document.getElementById("countdown").innerHTML = "at https://www.twitch.tv/pathofexile !";
    }
  }, 1000);
});
